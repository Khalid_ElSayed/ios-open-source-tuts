//
//  main.m
//  InfinitePagingScrollView
//
//  Created by Shardul Prabhu on 18/11/12.
//  Copyright (c) 2012 Shardul Prabhu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
